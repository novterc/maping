import Vue from 'vue'
import App from './App.vue'
import {AppWebSocket} from './classes/websocket'
import {CommonStorage} from './classes/commonStorage'
import {RequestMethods} from "./classes/requestMethods";
import {ReceiveMethods} from "./classes/receiveMethods";
import {CommandMap} from "./classes/commandMap";
import('./assets/styles/main.css');

Vue.config.productionTip = false;
// Vue.prototype.$webSockObj = new AppWebSocket();


let appWebSocket = new AppWebSocket();

window.sasatest = appWebSocket;
let commonStorage = new CommonStorage();
let commandMap = new CommandMap();
let requestMethods = new RequestMethods(appWebSocket, commonStorage);
let receiveMethods = new ReceiveMethods(commonStorage);
appWebSocket.addResponseListener((data) => { receiveMethods.handlerRequest(data); });


let mainObj = new Vue({
  provide: () => {
    return {
      webSockObj: appWebSocket,
      commonStorage: commonStorage,
      requestMethods: requestMethods,
      commandMap: commandMap,
    }
  },
  render: h => h(App),
});
mainObj.$mount('#app');
