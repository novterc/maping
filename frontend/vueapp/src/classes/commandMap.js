
class CommandMap {
    constructor() {
        this.test = {
            ch1: {
                _title: 'children 1',
                aaa: {
                    _execute: true,
                    _input: true,
                },
                bbb: true,
            },
            ch2: {
                aaa: true,
                bbb: true,
                ccc: true,
            },
            ch3: true,
            gh1: true,
            gh2: true,
            gh3: true,
        };
    }
}

export { CommandMap };