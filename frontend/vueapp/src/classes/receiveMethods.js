class ReceiveMethods {
    constructor(commonStorage) {
        this.commonStorage = commonStorage;
    }

    handlerRequest(request) {
        if( typeof this[request.method] == "function")
            this[request.method](request.data, request);
        else
            console.error('unsupported request method', request)
    }

    fail(data, request) {
        console.error({
            requestId: request.requestId,
            errorCode: request.errorCode,
            data: data
        });
    }

    mapProfiler(data) {
        this.commonStorage.profiler.login = data.login;
    }

    mapAppToken() {}

    mapUserSession(data) {
        document.cookie = "session=" + data.userSessionKey;
    }

    responseEcho() {}

}
export { ReceiveMethods };