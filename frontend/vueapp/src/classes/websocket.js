class AppWebSocket {
    constructor() {
        this.ws = null;
        this.waitConnect = false;
        this.appToken = '';
        this.requestInc = 0;
        this.commonData = {
            isConnected: false
        };
        this.onReadyCallbackList = [];
        this.waitResponseList = [];
        this.responseListenerList = [];
        // this.wsInit();
    }

    onReady(callback) {
        if (this.commonData.isConnected) {
            callback();
        } else {
            this.onReadyCallbackList.push(callback);
        }
    }

    onReadyCall() {
        this.onReadyCallbackList.map((callback) => {
            callback();
        });
        this.onReadyCallbackList = [];
    }

    wsRecoveryConnect() {
        let baseObj = this;
        if(this.waitConnect === false) {
            baseObj.waitConnect = true;
            let waitConnect = setInterval(() => {
                if (baseObj.commonData.isConnected) {
                    clearTimeout(waitConnect);
                    baseObj.waitConnect = false;
                } else {
                    baseObj.wsInit();
                }
            }, 1000);
            this.waitConnect = waitConnect;
        }
    }

    wsInit() {
        let baseObj = this;
        let ws = this.ws;

        ws = new WebSocket("ws://localhost:12359/?appToken=" + baseObj.appToken);

        ws.onopen = function() {
            console.log("OPEN ws");
            baseObj.commonData.isConnected = true;

            // baseObj.requestUltimate('lineToken', 'setOrCreate', {appToken: baseObj.appToken}, null,(data) => {
            //     baseObj.appToken = data.token;
            //     baseObj.onReadyCall();
            // });


        };

        ws.onclose = function() {
            console.log("CLOSE ws");
            baseObj.commonData.isConnected = false;
            ws = null;
            baseObj.wsRecoveryConnect();
        };

        ws.onmessage = function(evt) {
            try {
                let data = JSON.parse(evt.data);
                console.log('-->', data);
                baseObj.listenerResponse(data);
                baseObj.listenerResponseWaiteData(data);
            } catch (e) {
                console.error(e, evt.data);
            }
        };

        ws.onerror = function(evt) {
            console.error( evt.message);
        };

        baseObj.addResponseListener((request) => {
            if(request.method === 'mapAppToken') {
                baseObj.appToken = request.data.token;
                baseObj.onReadyCall();
            }
        });

        this.ws = ws;
    }

    sendMessage(message) {
        console.log('<--', message);
        this.ws.send(JSON.stringify(message));
    }

    sendRequester(requester) {
        this.sendMessage(requester.getSendObj());
    }

    sendRequesterIfReady(requester) {
        this.onReady(() => {
            this.sendRequester(requester);
        });
    }

    getRequestId() {
        this.requestInc++;
        return this.requestInc;
    }

    getRequesterObj(department, method, data, options = null, callbackResponse = null) {
       return new Requester(
           this.getRequestId(),
           department,
           method,
           data,
           options,
           callbackResponse
        );
    }

    preHandRequest(requesterObj) {
        if(requesterObj.callbackResponse !== null) {
            this.waitResponseList.push(requesterObj);
        }
    }

    requestSimple(department, method, data, options = null, callbackResponse = null) {
        let requesterObj = this.getRequesterObj(department, method, data, options, callbackResponse);
        this.preHandRequest(requesterObj);
        this.sendRequesterIfReady(requesterObj);
    }

    requestUltimate(department, method, data, options = null, callbackResponse = null) {
        let requesterObj = this.getRequesterObj(department, method, data, options, callbackResponse);
        this.preHandRequest(requesterObj);
        this.sendRequester(requesterObj);
    }

    listenerResponse(response) {
        this.responseListenerList.map((item) => {
            item(response);
        })
    }

    addResponseListener(callback) {
        this.responseListenerList.push(callback);
    }

    listenerResponseWaiteData(response) {
        let requesterObj = this.waitResponseList.find(function (requesterObj) {
            return (requesterObj.requestId === response.requestId);
        });
        if(typeof requesterObj !== "undefined") {
            console.log(requesterObj)
            requesterObj.callbackResponse(response.data);
            this.waitResponseList = this.waitResponseList.slice(this.waitResponseList.indexOf(requesterObj), 1);
        }
    }
}

class Requester {
    constructor(requestId, department, method, data, options, callbackResponse = null) {
        this.requestId = requestId;
        this.department = department;
        this.method = method;
        this.data = data;
        this.options = options;
        this.callbackResponse = callbackResponse;
    }

    getSendObj() {
        return {
            requestId: this.requestId,
            department: this.department,
            method: this.method,
            options: this.options,
            data: this.data,
        }
    }
}

export { AppWebSocket };
