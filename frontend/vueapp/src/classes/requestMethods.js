class RequestMethods {
    constructor(ws, commonStorage) {
        this.ws = ws;
        this.commonStorage = commonStorage;
    }

    getProfiler() {
        this.ws.requestSimple('security', 'getProfiler', null, {subscribe: true})
    }

    setProfiler(newLogin) {
        this.ws.requestSimple('security', 'setProfiler', {newLogin:newLogin}, null, (data) => {
            console.log('fail', data);
        });
    }

    setAuth(login, password) {
        this.ws.requestSimple('security', 'setAuth', {login:login, password:password});
    }

    testEcho() {
        this.ws.requestSimple('tests', 'echo', null);
    }
}
export { RequestMethods };