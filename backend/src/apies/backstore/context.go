package backstore

import (
	"cateringit/actions"
	"cateringit/events"
	"cateringit/repository"
	"cateringit/repository/userSessions"
)

type Context struct {
	ResponseCh  chan []byte
	ResponseInc int64
	IsConnect   bool
	Token       string
	Repository  *repository.Collection
	Actions     *actions.Actions
	EventMaster *events.Master
	IsGuest     bool
	UserId      int64
	UserSession *userSessions.UserSessions
}


func InitContext(
		newToken		string,
		responseCh		chan []byte,
		repository		*repository.Collection,
		actions			*actions.Actions,
		eventMaster		*events.Master,
	) *Context {
	return &Context {
		ResponseCh:  responseCh,
		ResponseInc: 0,
		Token:       newToken,
		IsConnect:   false,
		Repository:  repository,
		Actions:     actions,
		EventMaster: eventMaster,
		IsGuest:     true,
		UserId:      0,
	}
}
