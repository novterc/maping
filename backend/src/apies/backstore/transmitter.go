package backstore

import (
	"encoding/json"
	"log"
)


type RequestItem struct {
	RequestId		int64		`json:"requestId"`
	Department		string		`json:"department"`
	Method			string		`json:"method"`
	Options			RequestItemOptions `json:"options"`
	Data			interface{} `json:"data"`
}

type RequestItemOptions struct {
	Subscribe bool		 `json:"subscribe"`
}

type ResponseItem struct {
	RequestId		int64       `json:"requestId"`
	ResponseId		int64       `json:"responseId"`
	Method			string      `json:"method"`
	Options			interface{} `json:"options"`
	Data			interface{} `json:"data"`
	ErrorCode		int         `json:"errorCode"`
}


func (c *Context) SendResponseItem(responseItem ResponseItem) error {
	jsonData, err := json.Marshal(responseItem)
	if err != nil {
		return err
	} else {
		c.ResponseInc++
		c.ResponseCh <- []byte(string(jsonData))
		return nil
	}
}


func (c *Context) SendSuccess(requestId int64, method string, data interface{}) error {
	return c.SendResponseItem(ResponseItem {
		RequestId:  requestId,
		ResponseId: c.ResponseInc,
		Method:     method,
		ErrorCode:  0,
		Data:       data,
	})
}


func  (c *Context) SendFail(RequestId int64, errorCode int) error {
	err := c.SendResponseItem(ResponseItem{
		RequestId:  RequestId,
		ResponseId: c.ResponseInc,
		Method:     "fail",
		ErrorCode:  errorCode,
		Data:       nil,
	})

	if err != nil {
		log.Println("response error: ", err)
	}

	return err
}


func  (c *Context) SendConfirm(requestId int64) {
	var emptyData interface{}
	_ = c.SendSuccess(requestId, "confirm", emptyData)
}


func  (c *Context) SendConfirmByError(RequestId int64, err error, errorCode int) {
	if err == nil {
		c.SendConfirm(RequestId)
	} else {
		_= c.SendFail(RequestId, errorCode)
	}
}


func  (c *Context) SendConfirmByBoll(RequestId int64, isSuccess bool, errorCode int) {
	if isSuccess {
		c.SendConfirm(RequestId)
	} else {
		_= c.SendFail(RequestId, errorCode)
	}
}


func (c *Context) GetRequestData(request RequestItem, requestData interface{}) error {
	jsonData, errMarshal := json.Marshal(request.Data)
	if errMarshal == nil {
		errUnMarshal := json.Unmarshal(jsonData, &requestData)
		if errUnMarshal != nil {
			return errUnMarshal
		}
	} else {
		return errMarshal
	}
	return nil
}


func (c *Context) GetRequestDataOrSendFail(request RequestItem, requestData interface{}, errorCode int) error {
	err := c.GetRequestData(request, &requestData)
	if err != nil {
		_= c.SendFail(request.RequestId, errorCode)
		return err
	}
	return nil
}
