package apiResources

const (
	FailValidation		= 4003
	FailUpdate			= 4004
	NotAccess			= 4005

	FailGet				= 5001

	AlreadyAuth			= 6001
	FailAuth			= 6002
)