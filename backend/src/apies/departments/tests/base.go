package tests

import (
	"cateringit/apies/backstore"
	"log"
)

type Base struct {
	context 	*backstore.Context
}


func InitBase(context *backstore.Context) *Base {
	return &Base {
		context: context,
	}
}


func (base *Base) MethodRouterRequest(request backstore.RequestItem) {
	switch request.Method {

		case "echo":		base.Echo(request)

		default: log.Println("request method not support", request)
	}
}


func (base *Base) Echo(request backstore.RequestItem) {
	_= base.context.SendSuccess(request.RequestId, "responseEcho", "LO LO LO")
}
