package security

import (
	"cateringit/apies/backstore"
	"cateringit/apies/apiResources"
	"cateringit/repository/userSessions"
	"cateringit/repository/users"
	"log"
)

type Session struct {
	context		*backstore.Context
}


func InitSession(context *backstore.Context) *Session {
	return &Session {
		context:	context,
	}
}


func (session *Session) SetAuth(request backstore.RequestItem) {
	var requestData struct {
		Login string
		Password string
	}

	if session.context.GetRequestData(request, &requestData) != nil {
		_= session.context.SendFail(request.RequestId, apiResources.FailValidation)
		return
	}

	if len(requestData.Login) < 3 || len(requestData.Password) < 3 {
		_= session.context.SendFail(request.RequestId, apiResources.FailValidation)
		return
	}

	user, isSuccess, err := session.context.Actions.SecurityAuth(requestData.Login, requestData.Password)
	if err != nil {
		_= session.context.SendFail(request.RequestId, apiResources.FailGet)
		return
	}

	if isSuccess {
		userSession, userSessionKey, err := session.context.Actions.SecurityCreateUserSession(user.Id)
		if err != nil {
			log.Printf("sec create session: %v", err)
			_= session.context.SendFail(request.RequestId, apiResources.FailAuth)
			return
		}
		session.SetUserAndUserSession(user, userSession)
		session.OutUserSession(request.RequestId, userSessionKey)
	} else {
		_= session.context.SendFail(request.RequestId, apiResources.FailAuth)
	}
}


func (session *Session) SetUserAndUserSession(user *users.User, userSession *userSessions.UserSessions) {
	session.context.IsGuest = false
	session.context.UserSession = userSession
	session.context.UserId = user.Id
}


func (session *Session) outUserInfo(requestId int64, userId int64) {
	var responseData  struct {
		Id			int64 		`json:"id"`
		Login		string 		`json:"login"`
		IsGuest		bool 		`json:"isGuest"`
	}

	user, isNotFound, err := session.context.Repository.Users.FindById(userId)
	if err != nil || isNotFound{
		return
	}

	responseData.Id = user.Id
	responseData.Login = user.Login
	responseData.IsGuest = false

	_=  session.context.SendSuccess(requestId, "mapUserInfo", responseData)
}


func (session *Session) OutUserSession(requestId int64, userSessionKey string) {
	var responseData  struct {
		UserSessionKey		string 		`json:"userSessionKey"`
	}

	responseData.UserSessionKey = userSessionKey

	_=  session.context.SendSuccess(requestId, "mapUserSession", responseData)
}
