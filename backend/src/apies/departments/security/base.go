package security

import (
	"cateringit/apies/backstore"
	"log"
)

type Base struct {
	Session 	*Session
	Profile 	*Profile
	LineToken	*LineToken
}


func InitBase(context *backstore.Context) *Base {
	return &Base{
		Session:   InitSession(context),
		Profile:   InitProfile(context),
		LineToken: InitLineToken(context),
	}
}


func (main *Base) MethodRouterRequest(request backstore.RequestItem) {
	switch request.Method {

		case "setAuth":			main.Session.SetAuth(request)
		case "getProfiler":		main.Profile.OutProfiler(request.RequestId, request.Options.Subscribe)
		case "setProfiler":		main.Profile.SetProfiler(request)

		default: log.Println("request method not support", request)
	}
}
