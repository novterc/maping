package security

import (
	"cateringit/apies/apiResources"
	"cateringit/apies/backstore"
	"log"
)

type Profile struct {
	context 	*backstore.Context
}


func InitProfile(context *backstore.Context) *Profile {
	profile := Profile {
		context: context,
	}

	return &profile
}


func (profile *Profile) UpdateProfiler(request backstore.RequestItem) bool {
	var requestData struct {
		NewLogin string
	}

	if profile.context.IsGuest {
		log.Print("Warning UpdateProfiler: IsGuest")
		return false
	}

	if profile.context.GetRequestData(request, &requestData) != nil {
		log.Print("Warning UpdateProfiler: GetRequestData")
		return false
	}

	if len(requestData.NewLogin) < 3 {
		log.Print("Warning UpdateProfiler: invalid login")
		return false
	}

	r := profile.context.Repository.Users.SetUserInfo(profile.context.UserId, requestData.NewLogin)
	if r != nil {

		// TODO fix response update
		ro, er :=  r.RowsAffected()
		log.Printf("Error UpdateProfiler: SetUserInfo - %v %v",ro , er)
		return false
	}

	profile.context.SendConfirm(request.RequestId)
	return true
}


func (profile *Profile) OutProfiler(requestId int64, isSubscribe bool) {
	var responseData  struct {
		Id			int64 		`json:"id"`
		Login		string 		`json:"login"`
		IsGuest		bool 		`json:"isGuest"`
	}

	if profile.context.IsGuest {
		responseData.Id = 0
		responseData.Login = "Guest"
		responseData.IsGuest = true
	} else {
		user, isNotFound, err := profile.context.Repository.Users.FindById(profile.context.UserId)
		if err != nil || isNotFound {
			_= profile.context.SendFail(requestId, apiResources.FailGet)
			return
		}

		responseData.Id = user.Id
		responseData.Login = user.Login
		responseData.IsGuest = false
	}

	_= profile.context.SendSuccess(requestId, "mapProfiler", responseData)
}


func (profile *Profile) SetProfiler(request backstore.RequestItem) {
	profile.context.SendConfirmByBoll(
		request.RequestId,
		profile.UpdateProfiler(request),
		apiResources.FailUpdate,
	)
}
