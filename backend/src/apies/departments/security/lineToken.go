package security

import (
	"cateringit/apies/backstore"
)

type LineToken struct {
	context 	*backstore.Context
}


func InitLineToken(context *backstore.Context) *LineToken {
	return &LineToken {
		context: context,
	}
}


func (token *LineToken) OutLineToken(requestId int64) {
	var responseData  struct {
		Token		string 		`json:"token"`
	}

	responseData.Token = token.context.Token

	_=  token.context.SendSuccess(requestId, "mapAppToken", responseData)
}

