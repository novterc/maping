package apiesCore

import (
	"sync"
	"time"
)

const expireLine = 1

type masterLineCollection struct {
	mutex    sync.Mutex
	lineList []*Line
}


func initMasterLineCollection() *masterLineCollection {
	collect := &masterLineCollection{}
	go collect.runListenerExpireLine()

	return collect
}


func (collect *masterLineCollection) runListenerExpireLine() {
	collect.mutex.Lock()
	for {
		currentUnixTime := time.Now().Unix()
		for _,line := range collect.lineList {
			if line.IsConnect() == false && (currentUnixTime - line.GetAtDisconnected()) > expireLine  {
				line.SetIsExpired()
				collect.removeLine(line)
			}
		}

		time.Sleep(time.Second)
	}
	collect.mutex.Unlock()
}


func (collect *masterLineCollection) findLineByToken(token string) *Line {
	for _,line := range collect.lineList {
		if line.getToken() == token {
			return line
		}
	}

	return nil
}


func (collect *masterLineCollection) AddLine(line *Line) {
	collect.lineList = append(collect.lineList, line)
}


func (collect *masterLineCollection) removeLine(line *Line) {
	for i, itemLine := range collect.lineList {
		if line == itemLine {
			collect.lineList = collect.lineList[:i+copy(collect.lineList[i:], collect.lineList[i+1:])]
		}
	}
}
