package apiesCore

import (
	"cateringit/actions"
	"cateringit/apies/backstore"
	"cateringit/apies/departments/security"
	"cateringit/apies/departments/tests"
	"cateringit/events"
	"cateringit/repository"
	"cateringit/sweetness"
)

type Master struct {
	lineCollection		*masterLineCollection
	repository			*repository.Collection
	actions    			*actions.Actions
	eventMaster    		*events.Master
}


func InitMaster(
		repository 		*repository.Collection,
		actions 		*actions.Actions,
		eventMaster		*events.Master,
	) *Master {
	return &Master {
		lineCollection: initMasterLineCollection(),
		repository:     repository,
		actions:        actions,
		eventMaster:        eventMaster,
	}
}


func (master *Master) InitLine() *Line {
	responseCh := make(chan []byte)
	newToken := master.lineTokenGenerator()
	context := backstore.InitContext(
		newToken,
		responseCh,
		master.repository,
		master.actions,
		master.eventMaster,
	)
	line := &Line {
		atDisconnect: 0,
		context:	context,
		security:	security.InitBase(context),
		testBase:	tests.InitBase(context),
	}

	master.lineCollection.AddLine(line)

	return line
}


func (master *Master) InitLineByTokenOrSession(token string, session string) (error, *Line) {
	var line *Line
	if token != "" {
		line = master.lineCollection.findLineByToken(token)
		if line != nil {
			line.context.ResponseCh = make(chan []byte)
			return nil, line
		}
	}

	line = master.InitLine()

	if session != "" {
		userSession, userSessionIsEmpty, _ := master.repository.UserSessions.FindBySessionKey(session)
		if !userSessionIsEmpty {
			user, userIsEmpty, _ := master.repository.Users.FindById(userSession.UserId)
			if !userIsEmpty {
				line.security.Session.SetUserAndUserSession(&user, &userSession)
			}
		}
	}

	return nil, line
}


func (master *Master) lineTokenGenerator() string {
	for {
		token := sweetness.TokenGenerator(32)
		if master.lineCollection.findLineByToken(token) == nil {
			return token
		}
	}
}
