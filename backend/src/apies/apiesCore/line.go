package apiesCore

import (
	"cateringit/apies/backstore"
	"cateringit/apies/departments/security"
	"cateringit/apies/departments/tests"
	"encoding/json"
	"log"
	"time"
)

type Line struct {
	atDisconnect	int64
	context			*backstore.Context
	security		*security.Base
	testBase		*tests.Base
}


func (line *Line) GetResponseCh() chan []byte {
	return line.context.ResponseCh
}


func (line *Line) getToken() string {
	return line.context.Token
}


func (line *Line) GetAtDisconnected() int64 {
	return line.atDisconnect
}


func (line *Line) IsConnect() bool {
	return line.context.IsConnect
}


func (line *Line) SetAsConnected() {
	log.Printf("line as connect %v", line)
	line.context.IsConnect = true
	line.security.LineToken.OutLineToken(0)
	line.atDisconnect = 0
}


func (line *Line) SetAsDisconnected() {
	log.Printf("line as disconnect %v", line)
	if IsClosed(line.context.ResponseCh) {
		close(line.context.ResponseCh)
	}
	line.context.IsConnect = false
	line.atDisconnect = time.Now().Unix()
}

func IsClosed(ch <-chan []byte) bool {
	select {
	case <-ch:
		return true
	default:
	}

	return false
}


func (line *Line) SetIsExpired() {
	log.Printf("line is expired %v", line)
	line.SetAsDisconnected()
}


func (line *Line) ReadMessage(msgByte []byte) {
	var requestItem backstore.RequestItem
	err := json.Unmarshal(msgByte, &requestItem)
	if err != nil {
		log.Println("request Unmarshal", err, string(msgByte))
	} else {
		line.departmentRouterRequest(requestItem, msgByte)
	}
}
