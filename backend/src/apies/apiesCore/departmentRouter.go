package apiesCore

import (
	"cateringit/apies/backstore"
	"log"
)


func (line *Line) departmentRouterRequest(request backstore.RequestItem, msgByte []byte) {
	switch request.Department {

		case "security": line.security.MethodRouterRequest(request)
		case "tests": line.testBase.MethodRouterRequest(request)

		default: log.Println("request department not support", request)
	}
}
