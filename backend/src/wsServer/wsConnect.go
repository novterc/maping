package wsServer

import (
	"cateringit/apies/apiesCore"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
)

type WsConnect struct {
	wss				*WsServer
	connect			*websocket.Conn
	responseCh		chan []byte
	Id				int64
	apiesLine		*apiesCore.Line
}


func initWsConnect(connect *websocket.Conn, line *apiesCore.Line, connectInc int64, wss *WsServer) *WsConnect {
	wsc := WsConnect {
		wss:			wss,
		connect:		connect,
		responseCh:		line.GetResponseCh(),
		Id:				connectInc,
		apiesLine:		line,
	}

	return &wsc
}


func (wsc *WsConnect) closeHandler(code int, text string) error {
	log.Printf("closeHandler: %p %v",wsc,  wsc.Id)
	fmt.Println("close code: ", code)
	fmt.Println("close text: ", text)

	err := wsc.connect.Close()
	if err != nil {
		log.Printf("ERROR closeHandler: %p %v %s",wsc,  wsc.Id, err)
		return err
	} else {
		wsc.apiesLine.SetAsDisconnected()
		wsc.wss.RemoveConnect(wsc.Id)

		return nil
	}
}


func (wsc *WsConnect) listenReadMessage() {
	defer log.Printf("listenReadMessage close: %p %v",wsc,  wsc.Id)
	for {
		mt, msgByte, err := wsc.connect.ReadMessage()
		if err != nil {
			log.Printf("ERROR listenReadMessage: %v", err)
			break
		}

		log.Println("--> message: ",  wsc.Id, string(msgByte))

		if mt != 1 {
			log.Println("request message type not support ", string(msgByte))
		} else {
			wsc.apiesLine.ReadMessage(msgByte)
		}
	}
}


func (wsc *WsConnect) listenWriteMessage() {
	defer log.Printf("listenWriteMessage close: %p %v",wsc,  wsc.Id)
	for {
		message, ok := <- wsc.responseCh
		if !ok {
			return
		}
		log.Printf("<-- message: %p %v %v",wsc,  wsc.Id, string(message))
		err := wsc.SendByte(message)
		if err != nil {
			log.Printf("error message send. message:%#v | err: %#v", string(message), err)
		}
	}
}


func (wsc *WsConnect) SendMessage(message string) error {
	mt := 1
	return wsc.connect.WriteMessage(mt, []byte(message))
}


func (wsc *WsConnect) SendByte(msgByte []byte) error {
	mt := 1
	return wsc.connect.WriteMessage(mt, msgByte)
}
