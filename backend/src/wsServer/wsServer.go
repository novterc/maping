package wsServer

import (
	"cateringit/apies/apiesCore"
	"errors"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"sync"
)

type WsServer struct {
	addrListing 	string
	upgrader    	websocket.Upgrader
	apiesMaster 	*apiesCore.Master
	mutex       	sync.Mutex
	connectInc  	int64
	connectList 	[]*WsConnect
}


func InitWsServer(addrListing string, apiesMaster *apiesCore.Master) (*WsServer, error) {
	var wss = WsServer{
		addrListing:	addrListing,
		upgrader:		websocket.Upgrader {
			//ReadBufferSize:  1024,
			//WriteBufferSize: 1024,
		},
		apiesMaster: 	apiesMaster,
		connectInc:  	0,
	}
	wss.upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	http.HandleFunc("/", wss.initConnect)
	go http.ListenAndServe(wss.addrListing, nil)

	//TODO how get error from goroutine
	return &wss, nil
}


func (wss *WsServer) initConnect(responseWriter http.ResponseWriter, request *http.Request) {
	connect, upgradeErr := wss.upgrader.Upgrade(responseWriter, request, nil)
	if upgradeErr != nil {
		log.Print("upgrade:", upgradeErr)
		return
	}
	log.Print("create connect")

	apiesLineErr, apiesLine := wss.initApiesLineByRequest(request)
	if apiesLineErr != nil {
		_ = connect.Close()
		return
	}

	wsConnect := initWsConnect(connect, apiesLine, wss.connectInc, wss)
	connect.SetCloseHandler(wsConnect.closeHandler)

	wss.mutex.Lock()
	wss.connectInc++

	log.Printf("wss connectInc - %v - %v", wss.connectInc, len(wss.connectList))
	wss.connectList = append(wss.connectList, wsConnect)
	wss.mutex.Unlock()

	go wsConnect.listenWriteMessage()
	go wsConnect.listenReadMessage()

	apiesLine.SetAsConnected()
}


func (wss *WsServer) initApiesLineByRequest(request *http.Request) (error, *apiesCore.Line) {
	err, apiesLine := wss.apiesMaster.InitLineByTokenOrSession(
		getAppTokenByRequest(request),
		getCookieByRequest(request, "session"),
	)
	if err != nil {
		log.Print("initApiesLineByRequest:", err)
		return err, nil
	}
	if apiesLine == nil {
		return errors.New("apiesLine is nil"), nil
	}
	return nil, apiesLine
}


func (wss *WsServer) RemoveConnect(connId int64) {
	wss.mutex.Lock()
	var wsConnList = wss.connectList
	for i, conn := range wsConnList {
		if conn.Id == connId {
			wsConnList = wsConnList[:i+copy(wsConnList[i:], wsConnList[i+1:])]
			break
		}
	}
	wss.connectList = wsConnList
	wss.mutex.Unlock()
}


func getAppTokenByRequest(request *http.Request) string {
	keys, ok := request.URL.Query()["appToken"]
	log.Printf("appToken: %s", keys)

	if !ok || len(keys[0]) < 1 {
		return ""
	} else {
		return keys[0]
	}
}


func getCookieByRequest(request *http.Request, name string) string {
	for _, cook := range request.Cookies() {
		if cook.Name == name {
			return cook.Value
		}
	}
	return ""
}