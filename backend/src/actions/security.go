package actions

import (
	"cateringit/repository/userSessions"
	"cateringit/repository/users"
	"crypto/sha256"
	"fmt"
	"log"
	"math/rand"
)


func getPasswordHash(salt string, password string) string {
	h := sha256.New()
	passStr := salt + password
	h.Write([]byte(passStr))
	//return string(h.Sum(nil))

	return fmt.Sprintf("%x", h.Sum(nil))
}


func (action *Actions) SecurityAuth(login string, pass string) (*users.User, bool, error) {
	user, isNotFound, err := action.repository.Users.FindByLogin(login)
	if err != nil {
		return nil, false, err
	}

	if isNotFound == false {
		passHash := getPasswordHash(user.PasswordSalt, pass)
		//log.Printf("PasswordHash: %v", user.PasswordHash)
		//log.Fatalf("hash: %v", passHash)
		if passHash == user.PasswordHash {
			log.Printf("success auth: %v", login)
			action.eventMaster.Send("actions.security.auth.success", user.Id, user)
			return &user, true, nil
		} else {
			log.Printf("fail auth: %v", login)
		}
	}

	return nil, false, nil
}


func (action *Actions) SecurityUserSessionKeyGenerator() (string, error) {
	for {
		b := make([]byte, 32)
		rand.Read(b)
		key := fmt.Sprintf("%x", b)
		_, isNotFind, err := action.repository.UserSessions.FindBySessionKey(key)
		if err != nil {
			return "", err
		}
		if isNotFind {
			return key, nil
		}
	}
}


func (action *Actions) SecurityCreateUserSession(userId int64) (*userSessions.UserSessions, string, error) {
	key, err := action.SecurityUserSessionKeyGenerator()
	if err != nil {
		return nil, key, err
	}
	userSession, sessionErr := action.repository.UserSessions.Create(userId, key, true)

	action.eventMaster.Send("actions.security.userSession.create", userId, userSession)

	return &userSession, key, sessionErr
}
