package actions

import (
	"cateringit/events"
	"cateringit/repository"
	"github.com/jmoiron/sqlx"
)

type Actions struct {
	db				*sqlx.DB
	repository		*repository.Collection
	eventMaster		*events.Master
}

func Init(
		db 			*sqlx.DB,
		repository 	*repository.Collection,
		eventMaster *events.Master,
	) *Actions {
	action := Actions {
		db:				db,
		repository:		repository,
		eventMaster:	eventMaster,
	}
	return &action
}
