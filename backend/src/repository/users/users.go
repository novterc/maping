package users

import (
	"cateringit/repository/base"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"time"
)


type User struct {
	Id				int64		`db:"id"`
	Login			string		`db:"login"`
	PasswordHash	string		`db:"password_hash"`
	PasswordSalt	string		`db:"password_salt"`
	IsDeactivate	bool		`db:"is_deactivate"`
	CratedAt		time.Time	`db:"created_at"`
	UpdatedAt		time.Time	`db:"updated_at"`
}

type Repository struct {
	db			*sqlx.DB
	base		*base.BaseRepository
}


func InitRepository(db *sqlx.DB, base *base.BaseRepository) *Repository {
	return &Repository {
		db:		db,
		base:	base,
	}
}


func (u Repository) selectFirst(query string, args ...interface{}) (User, bool, error) {
	userList := make([]User, 0)
	err := u.db.Select(&userList, query, args...)
	if err != nil {
		return User {}, true, err
	}
	if len(userList) == 0 {
		return User {}, true, nil
	} else {
		return userList[0], false, nil
	}
}


func (u Repository) selectAll(query string, args ...interface{}) ([]User, bool, error) {
	userList := make([]User, 0)
	err := u.db.Select(&userList, query, args...)
	if err != nil {
		return []User {}, true, err
	}
	if len(userList) == 0 {
		return []User {}, true, nil
	} else {
		return userList, false, nil
	}
}


func (u Repository) SetUserInfo(userId int64, newLogin string) sql.Result {
	return u.db.MustExec("UPDATE users t1 SET t1.login = ? WHERE t1.id = ? ", newLogin, userId)
}


func (u Repository) FindById(id int64) (User, bool, error) {
	return u.selectFirst("SELECT * FROM users t1 WHERE t1.id = ? ", id)
}


func (u Repository) FindByLogin(login string) (User, bool, error) {
	return u.selectFirst("SELECT * FROM users t1 WHERE t1.login = ? ", login)
}


func (u Repository) FindAll() ([]User, bool, error) {
	return u.selectAll("SELECT * FROM users t1")
}
