package repository

import (
	"cateringit/repository/base"
	"cateringit/repository/userSessions"
	"cateringit/repository/users"
	"github.com/jmoiron/sqlx"
)

type Collection struct {
	Users			*users.Repository
	UserSessions	*userSessions.Repository
}


func InitCollection(db *sqlx.DB) *Collection {
	base := &base.BaseRepository {}
	collection := Collection {
		Users:			users.InitRepository(db, base),
		UserSessions:	userSessions.InitRepository(db, base),
	}

	return &collection
}
