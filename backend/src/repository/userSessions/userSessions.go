package userSessions

import (
	"cateringit/repository/base"
	"github.com/jmoiron/sqlx"
)

type UserSessions struct {
	Id				int64 	`db:"id"`
	UserId			int64	`db:"user_id"`
	SessionKey		string	`db:"session_key"`
	IsEnabled		bool	`db:"is_enabled"`
}

type Repository struct {
	db			*sqlx.DB
	base		*base.BaseRepository
}


func InitRepository(db *sqlx.DB, base *base.BaseRepository) *Repository {
	return &Repository {
		db:			db,
		base:		base,
	}
}


func (u Repository) selectFirst(query string, args ...interface{}) (UserSessions, bool, error) {
	userSessionsList := make([]UserSessions, 0)
	err := u.db.Select(&userSessionsList, query, args...)
	if err != nil {
		return UserSessions {}, true, err
	}
	if len(userSessionsList) == 0 {
		return UserSessions {}, true, nil
	} else {
		return userSessionsList[0], false, nil
	}
}


func (u Repository) selectAll(query string, args ...interface{}) ([]UserSessions, bool, error) {
	UserSessionsList := make([]UserSessions, 0)
	err := u.db.Select(&UserSessionsList, query, args...)
	if err != nil {
		return []UserSessions {}, true, err
	}
	if len(UserSessionsList) == 0 {
		return []UserSessions {}, true, nil
	} else {
		return UserSessionsList, false, nil
	}
}


func (u Repository) FindBySessionKey(sessionKey string) (UserSessions, bool, error) {
	return u.selectFirst(`
		SELECT * FROM user_sessions t1
		WHERE t1.session_key = ? 
		AND t1.is_enabled = 1 
	`, sessionKey)
}


func (u Repository) Create(userId int64, securityKey string, isEnabled bool) (UserSessions, error) {
	result, err := u.db.Exec(`
		INSERT INTO user_sessions (user_id, session_key, is_enabled) 
		VALUES (?, ?, ?)
	`, userId, securityKey, isEnabled)
	if err != nil {
		return UserSessions {}, err
	}

	lastId, lastErr := result.LastInsertId()
	if lastErr != nil {
		return UserSessions {}, lastErr
	}

	return UserSessions {
		Id:				lastId,
		UserId:			userId,
		SessionKey:		securityKey,
		IsEnabled:		isEnabled,
	}, nil
}
