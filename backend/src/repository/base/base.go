package base

import (
	"log"
)

type BaseRepository struct {}


func (base BaseRepository) LogFatal(err error){
	if(err != nil) {
		log.Fatal(err)
	}
}