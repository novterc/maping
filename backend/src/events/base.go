package events

type Master struct {
	listenerIdInc int64
	listenerList  map[int64]*EventListener
}

type EventListener struct {
	Id      	int64
	Channel 	chan EventMessage
	ListenKey	string
	ListenId	int64
}

type EventMessage struct {
	Val			interface{}
}


func InitMaster() *Master {
	master := Master{
		listenerIdInc: 	1,
		listenerList: 	make(map[int64]*EventListener),
	}
	return &master
}


func (master *Master) CreateListener(listenKey string, listenId int64) *EventListener {
	master.listenerIdInc++
	eventLine := EventListener {
		Id:      	master.listenerIdInc,
		Channel: 	make(chan EventMessage),
		ListenKey: 	listenKey,
		ListenId: 	listenId,
	}
	master.listenerList[master.listenerIdInc] = &eventLine
	return &eventLine
}


func (master *Master) DeleteListener(listener *EventListener) {
	master.DeleteListenerById(listener.Id)
}


func (master *Master) DeleteListenerById(id int64) {
	close(master.listenerList[id].Channel)
	delete(master.listenerList, id)
}


func (master *Master) Send(key string, id int64, value interface{}) {
	for _, v := range master.listenerList {
		if v.ListenKey == key && (v.ListenId == 0 || v.ListenId == id) {
			v.Channel <- EventMessage {
				Val: 	value,
			}
		}
	}
}
