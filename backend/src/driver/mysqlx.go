package driver

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/jmoiron/sqlx"
)


func GetConnectionMysqlX(dataSource string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", dataSource)

	return db, err
}
