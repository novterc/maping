package resources

const (
	DefaultMysql		= "root:password@tcp(127.0.0.1:3306)/combain?parseTime=true"
	DefaultWsServerPort	= "localhost:12359"
)