package sweetness

import (
	"crypto/rand"
	"fmt"
)

func TokenGenerator(length int) string {
	for {
		b := make([]byte, length)
		rand.Read(b)
		return fmt.Sprintf("%x", b)
	}
}
