package common

import (
	"cateringit/actions"
	"cateringit/apies/apiesCore"
	"cateringit/driver"
	"cateringit/events"
	"cateringit/repository"
	"cateringit/resources"
	"cateringit/wsServer"
	"github.com/jmoiron/sqlx"
	"log"
)

type App struct {
	Db			*sqlx.DB
	Repository	*repository.Collection
	Wss			*wsServer.WsServer
	Actions		*actions.Actions
	ApiesMaster	*apiesCore.Master
	EventMaster	*events.Master
}


func InitAppAndRun() *App {
	app := &App {}

	var dbErr error
	app.Db, dbErr = driver.GetConnectionMysqlX(resources.DefaultMysql)
	if dbErr != nil {
		log.Fatal(dbErr)
	} else {
		log.Println("Default DB success connected")
	}

	app.EventMaster = events.InitMaster()
	app.Repository = repository.InitCollection(app.Db)
	app.Actions = actions.Init(app.Db, app.Repository, app.EventMaster)
	app.ApiesMaster = apiesCore.InitMaster(app.Repository, app.Actions, app.EventMaster)

	var wssErr error
	app.Wss, wssErr = wsServer.InitWsServer(resources.DefaultWsServerPort, app.ApiesMaster)
	if wssErr != nil {
		log.Fatal(wssErr)
	}

	//go logEventListen(app.EventMaster, "createUserSession" ,"actions.createUserSession", 0)

	return app
}


func logEventListen (master *events.Master, title string, key string, id int64) {
	listener := master.CreateListener(key, id)

	for {
		message, ok := <-listener.Channel
		if !ok {
			return
		}

		log.Printf("Event " + title + " : %v", message.Val)
	}
}

